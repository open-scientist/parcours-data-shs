# Parcours data SHS

Lancez le notebook en cliquant sur le bouton [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/open-scientist%2Fparcours-data-shs/master?urlpath=lab%2Ftree%2Fnotebooks%2Fsession10%2Findex.ipynb)

Additions à la session git rapide
```
git config --global user.email EMAIL
git config --global user.name NAME
git config --global credential.helper 'cache --timeout 7200'
```
